<?php 

require_once './utils/db.php';
require_once './utils/fonctions.php';

$connexion = getConnexion();
//announcesFictifs($connexion) ;

$lastAnnounces = getLastAnnounces(12, $connexion) ;
$categories = getAllCategories($connexion);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Expad Thiès</title>
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
    <?php include_once './partials/header.php'; ?>
    <div class="container mt-5">
        <form action="" method="post">
           <div class="row col-md-12">
                <div class="form-group col-md-6">
                <input type="text"
                       class="form-control form-control-lg"
                       placeholder="Ex : iPhone 13 pro Max" 
                       name="search" 
                       id="">
            </div>
                
            <div class="form-group col-md-3">
            <select 
                    name="category" 
                    class="col-md-3 form-select form-select-lg"
                    id="">
                    <option>Choisissez une catégorie</option>
                    <?php foreach($categories as $category) : ?>
                        <option value="<?= $category['id'] ?>">
                            <?= $category['name'] ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            
            <div class="form-group col-md-3 d-grid gap-2">
            <button 
                    type="submit"
                    class="btn btn-primary btn-lg">
                    Rechercher
                </button>
            </div>
           </div> 
        </form>
    </div>


    <div class="container-fluid mt-5" style="margin:10px;">
        <div class="row col-md-12">
            <?php if(isset($_GET['success'])) : ?>
                <div class="alert alert-success">
                    Annonce supprimé avec succès !!!
                </div>
            <?php endif; ?>
            <?php foreach($lastAnnounces as $announce) : ?>
                <div class="card col-md-4">
                    <img src="<?= $announce['image'] ?>" 
                    alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $announce['title']; ?></h5>
                        <p class="card-text">
                        <?= $announce['description'] ?>
                        </p>
                        <p class="card-text">
                        <?= $announce['price'] ?> FCFA
                        </p>
                        <a href="#" class="bt btn-primary">En savoir plus</a>
                        <a href="<?= $base_url."admin/announces/edit.php?id=".$announce['id'] ?>" class="btn btn-warning btn-sm">Modifier</a>
                        <a onclick="return confirm('Êtes-vous sûr ?')" href="<?= $base_url."admin/announces/delete.php?id=".$announce['id']; ?>" class="btn btn-danger btn-sm">Supprimer</a>
                    </div>
                </div>
            <?php endforeach; ?>

            <div class="row mt-2 text-center position-relative">
            <nav aria-label="Page navigation example">
                <ul class="pagination position-absolute top-50 start-50">
                    <li class="page-item"><a class="page-link" href="#">Précédent</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Suivant</a></li>
                </ul>
            </nav>
            </div>
        </div>
    </div>

    <?php include './partials/footer.php'; ?>

    <script src="./assets/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>