<?php 
require_once '../../utils/db.php';
require_once '../../utils/fonctions.php';

$connexion = getConnexion();
$categories = getAllCategories($connexion);

$success = false ;
//var_dump($_SERVER["REQUEST_METHOD"]);
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    $titre = $_POST['title'] ?? "" ;
    $price = $_POST['price'] ?? 0 ;
    $description = $_POST['description'] ?? "";
    $image = $_POST['image'] ;
    $category_id = $_POST['category_id'] ?? 1 ;
    if($titre != ""){
        if(insertAnnounce(
            $connexion,$titre,$price,$description,$category_id,$image
            )){
            $success = true;
        }
    }
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter une annonce</title>
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
    <?php include_once "../../partials/header.php" ?>
    
    <div class="container mt-3">
        <h3>Formulaire de création d'une annonce</h3>
        <?php if($success == true) : ?>
            <div class="alert alert-success">
                Annonce ajoutée avec succès !!!!
            </div>
        <?php endif; ?>
        <form action="" method="post">
            <?php include_once 'form.php'; ?>
            <div class="d-grid gap-2">
            <button
                type="submit"
                class="btn btn-outline-primary"
            >
                Ajouter cette annonce
            </button>
            </div>
            
            
        </form>
    </div>

<script src=".../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>

<?php include_once "../../partials/footer.php"; ?>
</body>
</html>