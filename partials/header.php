<?php $base_url = "http://localhost/ut2024/expat-thies/"; ?>
<nav class="navbar navbar-expand-lg bg-primary border-bottom 
            border-body" data-bs-theme="dark">
    <div class="container-fluid">
        <a href="<?= $base_url; ?>" class="navbar-brand">
            EXPAD THIES
        </a>
        <button class="navbar-toggler" type="button" 
        data-bs-toggle="collapse" 
        data-bs-target="#navbarSupportedContent" 
        aria-controls="navbarSupportedContent" 
        aria-expanded="false" 
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navbarSupportedContent" class="collapse navbar-collapse">
            <ul class="navbar-nav me-auto mb-2">
                <li class="nav-item">
                    <a href="<?= $base_url; ?>" class="nav-link active">
                        Accueil
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= $base_url."admin/announces/add.php"?>" class="nav-link">
                        Ajouter une annonce
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>