<?php 


//Récupérer les 12 derniers annonces
function getLastAnnounces($limit, $connexion){

    //La requête 
    $query = "SELECT * FROM announces ORDER BY id DESC LIMIT $limit" ;

    ///2ieme 
    $statement = $connexion->prepare($query) ;
    $statement->execute();

    //3ieme, on récupère le résultat
    $resultats = $statement->fetchALL(PDO::FETCH_ASSOC) ;

    return $resultats ;
}


function getAnnounceById($id, $connexion){

    //La requête 
    $query = "SELECT * FROM announces WHERE id=$id" ;

    ///2ieme 
    $statement = $connexion->prepare($query) ;
    $statement->execute();

    //3ieme, on récupère le résultat
    $resultat = $statement->fetch(PDO::FETCH_ASSOC) ;

    return $resultat ;
}

function getCategoryById($id, $connexion){

    //La requête 
    $query = "SELECT * FROM categories WHERE id=$id" ;

    ///2ieme 
    $statement = $connexion->prepare($query) ;
    $statement->execute();

    //3ieme, on récupère le résultat
    $resultat = $statement->fetch(PDO::FETCH_ASSOC) ;

    return $resultat ;
}


function getAllCategories($connexion){

    //La requête 
    $query = "SELECT * FROM categories" ;

    ///2ieme 
    $statement = $connexion->prepare($query) ;
    $statement->execute();

    //3ieme, on récupère le résultat
    $resultats = $statement->fetchALL(PDO::FETCH_ASSOC) ;

    return $resultats ;
}

function announcesFictifs($connexion){
    for ($i=1; $i <= 50; $i++) { 
        
        $query = "INSERT INTO announces 
            (title, price, description, category_id) 
            VALUES (?, ?, ?, ?)" ;
        
        $data = [
            "Annonce n° $i",
            rand(1000, 10000),
            "Ceci est la description de l'annonce n° $i",
            rand(1, 4)
        ] ;
        
        $statement = $connexion->prepare($query) ;
        $statement->execute($data) ;

        
    }

    echo "Great !!";
}

function insertAnnounce($connexion, $title,$price, $desc, $cat, $img){
    $query = "INSERT INTO announces 
            (title, price, description, category_id, image) 
            VALUES (?, ?, ?, ?, ?)" ;
        
        $data = [
            $title,
            $price,
            $desc,
            $cat,
            $img
        ] ;
        
        $statement = $connexion->prepare($query) ;
        $statement->execute($data) ;
        return true;
}


function updateAnnounce($connexion,$id, $title,$price, $desc, $cat, $img){
    $query = "UPDATE `announces` SET
    title=?, price=?, description=?,
    image=?,category_id=? WHERE id=$id" ;
        
        $data = [
            $title,
            $price,
            $desc,
            $img,
            $cat,
            
        ] ;
        
        $statement = $connexion->prepare($query) ;
        $statement->execute($data) ;
        return true;
}