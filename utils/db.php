<?php 

function getConnexion(
    $servername="localhost",
    $dbname = "uidt_annonce_thies",
    $username ="root",
    $password = "")
{
    //Supprimer toutes les connexions antérieures
    $connexion = null;
    try {
        $dsn = "mysql:host=$servername;dbname=$dbname";
        $connexion = new PDO($dsn, $username, $password) ;
        $connexion->setAttribute(PDO::ATTR_ERRMODE, 
                                PDO::ERRMODE_EXCEPTION);

        //echo "Connexion réussie" ;
    } catch (PDOException $e) {
        echo "Erreur : ". $e->getMessage();
    }

    return $connexion;
}